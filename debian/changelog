libc++ (6.0.1-1) unstable; urgency=medium

  * New upstream release

 -- Sylvestre Ledru <sylvestre@debian.org>  Sun, 01 Jul 2018 15:43:20 +0200

libc++ (6.0-3) unstable; urgency=medium

  * Remove some useless patches found by lintian
    patch-file-present-but-not-mentioned-in-series

 -- Sylvestre Ledru <sylvestre@debian.org>  Sat, 17 Mar 2018 17:05:12 +0100

libc++ (6.0-2) unstable; urgency=medium

  * Only run the testsuites on amd64, arm64 and i386

 -- Sylvestre Ledru <sylvestre@debian.org>  Sun, 11 Mar 2018 17:37:11 +0100

libc++ (6.0-1) unstable; urgency=medium

  * New upstream release
  * Packaging moved to debian salsa
  * Build using clang 6 instead of 5
  * Add a watch file
  * Build with the glibc 2.27 (Closes: #891679)

 -- Sylvestre Ledru <sylvestre@debian.org>  Sun, 11 Mar 2018 11:47:45 +0100

libc++ (5.0.1-2) unstable; urgency=medium

  * Disable the execution of the testsuite on mips*

 -- Sylvestre Ledru <sylvestre@debian.org>  Thu, 18 Jan 2018 22:33:13 +0100

libc++ (5.0.1-1) unstable; urgency=medium

  * New upstream release
  * Standards-Version updated to 4.1.3

 -- Sylvestre Ledru <sylvestre@debian.org>  Sun, 14 Jan 2018 18:44:22 +0100

libc++ (5.0-1) unstable; urgency=medium

  * New upstream release
  * Standards-Version updated to 4.1.1
  * Update of the descriptions (were only mentionning C++11)
  * Fix priority-extra-is-replaced-by-priority-optional
  * Fix debian-rules-sets-dpkg-architecture-variable
  * glibc2.26.diff removed (fixed upstream)

 -- Sylvestre Ledru <sylvestre@debian.org>  Fri, 24 Nov 2017 18:53:01 +0100

libc++ (4.0.1-3) unstable; urgency=medium

  [ Matthias Klose ]
  * Ignore one more failing test on arm64

 -- Sylvestre Ledru <sylvestre@debian.org>  Fri, 24 Nov 2017 17:29:58 +0100

libc++ (4.0.1-2) unstable; urgency=medium

  * Silent libcxx/atomics/atomics.align/align.pass.sh.cpp
    which fails on ppc64el
  * Silent thread.condition/thread.condition.condvar/wait_for.pass.cpp
    which fails on arm64

  [ Matthias Klose ]
  * Don't include xlocale.h. (glibc)

 -- Sylvestre Ledru <sylvestre@debian.org>  Fri, 10 Nov 2017 12:10:53 +0100

libc++ (4.0.1-1) unstable; urgency=medium

  * New upstream release
  * Update the llvm/clang dep to 4.0 (Closes: #873423)
  * Standards-Version to 4.0.0
  * Remove copyright-year-in-future (false positive)

 -- Sylvestre Ledru <sylvestre@debian.org>  Sat, 26 Aug 2017 20:37:36 +0200

libc++ (3.9.1-3) unstable; urgency=medium

  * Fix a typo in debian/control (Closes: #870440)

 -- Sylvestre Ledru <sylvestre@debian.org>  Wed, 02 Aug 2017 06:40:23 +0200

libc++ (3.9.1-2) unstable; urgency=medium

  * Silent a few tests on some archs to make the testsuite green.
    The testsuites are more than 5000 tests. We can live with ignoring a few
    tests.
    (Closes: #823058) even if it was a different issue
  * Remove useless libc++1.postinst, libc++1.postrm, libc++abi1.postinst
    and libc++abi1.postrm
  * Add debian/watch to detect new upstream release (but should
    not be used to retrieve sources)

 -- Sylvestre Ledru <sylvestre@debian.org>  Tue, 10 Jan 2017 16:45:20 +0100

libc++ (3.9.1-1) unstable; urgency=medium

  * New upstream release
  * Fix the FTBFS with dpkg-buildpackage -A (Closes: #849570)
    Thanks to Santiago Vila for the patch

 -- Sylvestre Ledru <sylvestre@debian.org>  Thu, 29 Dec 2016 11:09:43 +0100

libc++ (3.9.0-3) unstable; urgency=medium

  * Team upload
  * Add missing Replaces dependency to libc++abi-dev (Closes: #843027)
  * Fix arm EHABI code to allow exceptions on armhf
  * Fix some unit test on different architectures

 -- Pauli <suokkos@gmail.com>  Thu, 03 Nov 2016 23:49:34 +0200

libc++ (3.9.0-2) unstable; urgency=medium

  * Team upload
  * Fix libc++abi-dev header paths (Closes: #800492, #806692)
  * Use cmake build system that should be easier to port to newer versions
  * Build libc++experimental.a for c++17 features with unstable ABI
  * Remove hardcoded ldconfig invocations from postinst&postrm
  * Bugs fixed with 3.9.0 release (Closes: #808086, #801303, #841594)

  [ Sylvestre Ledru ]
  * Remove pyc from the deb packages
  * Fix the copyright file

 -- Pauli <suokkos@gmail.com>  Tue, 01 Nov 2016 17:27:02 +0200

libc++ (3.9.0-1) unstable; urgency=medium

  * Team upload
  * New upstream release (Closes: #837608)

  [ Sylvestre Ledru ]
  * Standards-Version updated to 3.9.8

 -- Pauli <suokkos@gmail.com>  Mon, 31 Oct 2016 12:56:19 +0200

libc++ (3.7.0-1) unstable; urgency=medium

  * New upstream release

 -- Sylvestre Ledru <sylvestre@debian.org>  Sun, 06 Sep 2015 12:02:37 +0200

libc++ (3.6.2-1) unstable; urgency=medium

  * New upstream release
  * Upload in unstable

 -- Sylvestre Ledru <sylvestre@debian.org>  Mon, 03 Aug 2015 14:23:53 +0200

libc++ (3.6-1~exp1) experimental; urgency=medium

  * New upstream release
  * Add support for kfreebsd. Thanks to Jan Henke (Closes: #780106)
  * Standards-Version updated to 3.9.6

 -- Sylvestre Ledru <sylvestre@debian.org>  Tue, 17 Mar 2015 18:48:20 +0100

libc++ (3.5-2) unstable; urgency=medium

  * Remove a useless patch libcxx-use_libcxxabi.patch (Closes: #762354)
  * g++-libc++ helper now manages option -pthread (Closes: #760542)
    Thanks to Miles Bader for the patch
  * Fix the FTBFS under ARM/ARMHF by disabling the ARM EHABI exception handling
    (Closes: #761193)
  * Fails the build in case of error
  * Fix some files layout issues

 -- Sylvestre Ledru <sylvestre@debian.org>  Sun, 28 Sep 2014 23:01:19 +0200

libc++ (3.5-1) unstable; urgency=medium

  * New upstream release
  * Now that libcxxabi is also official released, use stable releases
  * Update of the repack script
  * syntax-error-in-dep5-copyright fixed
  * Depends on clang instead of clang-X.Y

 -- Sylvestre Ledru <sylvestre@debian.org>  Wed, 03 Sep 2014 16:32:53 +0200

libc++ (1.0~svn216840-1) unstable; urgency=medium

  * New snapshot release
  * Update the dependency from clang-3.4 to clang (co install)
    (Closes: #759938)

 -- Sylvestre Ledru <sylvestre@debian.org>  Sun, 31 Aug 2014 11:32:59 +0200

libc++ (1.0~svn205159-1) unstable; urgency=medium

  * New snapshot release
  * Force the usage of clang 3.4 (Closes: #736991)

 -- Sylvestre Ledru <sylvestre@debian.org>  Sun, 30 Mar 2014 21:26:14 +0200

libc++ (1.0~svn199600-1) unstable; urgency=low

  * New snapshot release
  * Standards-Version updated to 3.9.5
  * Introduce package libc++-helpers (Closes: #718637)
  * Rename libc++-src => libc++-test
  * Rename libc++abi-src => libc++abi-test
  * For both, remove the sources from the packages (apt-get source libc++)

 -- Sylvestre Ledru <sylvestre@debian.org>  Sun, 19 Jan 2014 15:25:24 +0100

libc++ (1.0~svn196040-1) unstable; urgency=low

  * New snapshot release

 -- Sylvestre Ledru <sylvestre@debian.org>  Sun, 01 Dec 2013 11:00:53 +0100

libc++ (1.0~svn189766-1) unstable; urgency=low

  * New snapshot release

 -- Sylvestre Ledru <sylvestre@debian.org>  Mon, 02 Sep 2013 19:43:49 +0200

libc++ (1.0~svn181765-1) unstable; urgency=low

  * New snapshot release
  * Upload to unstable

 -- Sylvestre Ledru <sylvestre@debian.org>  Tue, 14 May 2013 10:41:44 +0200

libc++ (1.0~svn180177-1~exp1) experimental; urgency=low

  * New upstream release
  * Minimal clang version (>= 3.2)
  * Standards-Version updated to version 3.9.4

 -- Sylvestre Ledru <sylvestre@debian.org>  Wed, 24 Apr 2013 09:32:41 +0200

libc++ (1.0~svn170866-1~exp1) experimental; urgency=low

  * New snapshot release
  * libc++-dev now provides libstdc++-dev
  * Update of the homepage
  * Update the minimal version of debhelper (9)

 -- Sylvestre Ledru <sylvestre@debian.org>  Fri, 21 Dec 2012 09:45:40 +0100

libc++ (1.0~svn161106-1~exp1) experimental; urgency=low

  * New snapshot release

  [ Andrej Belym ]
  * Multiarch support.

 -- Sylvestre Ledru <sylvestre@debian.org>  Wed, 01 Aug 2012 10:38:08 +0200

libc++ (1.0~svn160132-1~exp1) experimental; urgency=low

  * Initial release (Closes: #682245)

 -- Andrej Belym <white.wolf.2105@gmail.com>  Sat, 02 Jun 2012 19:25:47 +0400
